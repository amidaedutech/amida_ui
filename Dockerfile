
FROM node as build-step


WORKDIR /usr/src/app

COPY package.json ./

COPY . /usr/src/app

RUN npm run build --prod

# Stage 2

FROM nginx

EXPOSE 80

COPY --from=build-step /usr/src/app/dist/ /usr/share/nginx/html


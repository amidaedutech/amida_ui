import { UserService } from "app/shared/services/user.service";
import { ApiService } from "app/shared/services/api.service";
import { Component, OnInit, AfterViewInit, Renderer2 } from "@angular/core";
import { Title } from "@angular/platform-browser";
import {
  Router,
  NavigationEnd,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from "@angular/router";

import { RoutePartsService } from "./shared/services/route-parts.service";
import { ThemeService } from "./shared/services/theme.service";

import { filter } from "rxjs/operators";
import { LayoutService } from "./shared/services/layout.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, AfterViewInit {
  appTitle = "Amida";
  pageTitle = "";
  socket: any;
  constructor(
    public title: Title,
    private api: ApiService,
    private user: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private routePartsService: RoutePartsService,
    private themeService: ThemeService,
    private layout: LayoutService,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.socket = this.api.socket;
    this.socket.on("amida:client:logout", (data) => {
      let user = this.user.user();
      let token = localStorage.getItem("access_token");
      if (data._id === user._id && data.token !== token) {
        localStorage.removeItem("user");
        localStorage.removeItem("access_token");
        this.router.navigate(['/sessions/signin']);
      }
    });
    // if(this.user.user()._id){
    //   this.user.refreshToken().subscribe();
    // }
   
    this.changePageTitle();
  }
  ngAfterViewInit() {}
  changePageTitle() {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((routeChange) => {
        var routeParts = this.routePartsService.generateRouteParts(
          this.activeRoute.snapshot
        );
        if (!routeParts.length) return this.title.setTitle(this.appTitle);
        // Extract title from parts;
        this.pageTitle = routeParts
          .reverse()
          .map((part) => part.title)
          .reduce((partA, partI) => {
            return `${partA} > ${partI}`;
          });
        this.pageTitle += ` | ${this.appTitle}`;
        this.title.setTitle(this.pageTitle);
      });
  }
}

import { Component, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { egretAnimations } from "../../../shared/animations/egret-animations";
import { Subscription } from 'rxjs';
import { ApiService } from '../../../shared/services/api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
declare var Razorpay: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  animations: egretAnimations
})
export class CheckoutComponent implements OnInit, OnDestroy {
  cartList= [];
  cartDetails: any;
  public total: number = 0;
  public subTotal: number = 0;
  public vat: number = 18;
  private initpaymentData:any;
  public subs1: Subscription;
  public subs2: Subscription;
  public subs3: Subscription;
  private clientUrl = '';  
  constructor( private api: ApiService,
    private loader: AppLoaderService,
    public sanitizer: DomSanitizer,
    private changeDetectorRef: ChangeDetectorRef,
    private appear: AppConfirmService,
    private router: Router,
  ) {
this.clientUrl = document.location.protocol + '//' + document.location.hostname + ':8080/';
  }

  ngOnInit() {
    this.getCart();
  }
  ngOnDestroy() {
    if (this.subs1) {
      this.subs1.unsubscribe();
    }
   if (this.subs2) {
      this.subs2.unsubscribe();
    }
    if(this.subs3) {
      this.subs3.unsubscribe();
    }
  }
  getCart() {
    let user = JSON.parse(localStorage.getItem('user'));
    if ((user ||{})._id) {
      this.loader.open();
      this.subs1 = this.api.getCartList()
        .subscribe(res => {
          if (res && res.courses && res.courses.length > 0) {
            this.cartDetails = res;
            this.cartList = res.courses;
            let courseList = [];
            let tot = 0;
            for(let item of this.cartList) {
              // let path = item.image;
              // path = path.substr(16)
              // item.image =  this.clientUrl + path;
            
              courseList.push(item);
              if ((item ||{}).price) {
                tot += item.price;
                this.subTotal += item.price;
              }
            }
            let tax = tot * (18/100);
            this.total = tot + tax;
            this.cartDetails.courses = courseList;
          }else {
            this.cartList = [];
          }
          this.changeDetectorRef.detectChanges();
          this.loader.close();
        }, err => {
          this.loader.close();
          this.confirmMsg('Fail', err.error);
        });
    } else {
      this.confirmMsg('Validation', 'Please signin to enter cart list!!');
    }
  }

  placeOrder() {
    let scope = this;
    const user = JSON.parse(localStorage.getItem("user"));
    let razorPayOptions = {
      key: '',
      currency: 'INR',
      description: 'Payment from Amida | Educational Services',
      amount: 0,
      order_id: '',
      handler: (scope)=>{

      },
      "prefill": {
        "name": user.firstName,
        "email": user.email,
        "contact": user.phone
      },
      "notes": {
          "address": "Payment from Amida Office"
      },
      "theme": {
          "color": "#3399cc"
      } 
    };
    
    this.subs2 = this.api.initPayment().subscribe(
      res => {
        this.initpaymentData = res;
        razorPayOptions.key=res.r_key_id;
        razorPayOptions.amount=res.amount.toFixed(4);
        razorPayOptions.order_id=res.id;
        razorPayOptions.handler = (response) => {
          this.razorPayResponseHandler(response, res);
        } ;
        var rzpl = new Razorpay(razorPayOptions);
        rzpl.on('payment.failed', (response) =>{
          this.razorPayResponseHandler(response, res)
        });
        rzpl.open();   
      } ,
      err =>  this.confirmMsg('Fail', err.error)
    );
  }

  razorPayResponseHandler(res, order){
    let mergedValue = {...res, ...order};
    console.log('mergedValue', mergedValue);
    this.subs3 = this.api.rpayResponsePayment(mergedValue).subscribe(res => {
     // {'status': req.body.status, 'order':'success', txnid: req.body.txnid}
     this.router.navigate(['/course/paymentdone'],{ queryParams: res});
     setTimeout(() => {
      location.reload();
     }, 100);
    } , err =>  {
      setTimeout(() => {
        location.reload();
       }, 100);
       this.router.navigate(['/course/paymentdone'], { queryParams: err});
    });
  }

  confirmMsg(title, msg) {
    this.appear.confirm({ title: title, message: msg, button: 'close' }).subscribe(res => { });
  }
}

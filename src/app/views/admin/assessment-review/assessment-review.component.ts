import { Component, OnInit, OnDestroy, ViewChild, ElementRef, ViewEncapsulation, ChangeDetectorRef, HostListener } from '@angular/core';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { ApiService } from '../../../shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { Observable } from 'rxjs/Observable';
import { MatSidenav } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-assessment-review',
  templateUrl: './assessment-review.component.html',
  styleUrls: ['./assessment-review.component.scss'],
  animations: [egretAnimations]
})
export class AssessmentReviewComponent implements OnInit {
  @HostListener('window:beforeunload', ['$event'])
  public currentQSelected = 1;
  public questionNumber: any;
  public questionMatrix: any;
  public qOptions = [];
  public user: any;
  public assessmentDetail: any;
  public assResult: any;
  public answerMatrix = [];
  public questions = [];
  public qAnswer = '';
  public comments = '';
  public answeredAt = 0;
  public ass_id: any;
  public isSideNavOpen: boolean;
  public currentPage: any;
  ans: boolean = false;
  score: any;
  grade: any;
  public subs1: Subscription;
  public subs2: Subscription;
  public subs3: Subscription;
  @ViewChild(MatSidenav, { static: false }) private sideNav: MatSidenav;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private loader: AppLoaderService,
    private api: ApiService,
    private appear: AppConfirmService,
    private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) {
      this.route.queryParams.subscribe(params => {
        this.ass_id = params.id;
        this.getAssessmentById({ _id: params.id }, 1);
      });
    }
  }
  ngOnDestroy() {
    if (this.subs1) {
      this.subs1.unsubscribe();
    }
    if (this.subs2) {
      this.subs2.unsubscribe();
    }
    if (this.subs3) {
      this.subs3.unsubscribe();
    }
  }
  canDeactivate(): Observable<boolean> | boolean {
    // insert logic to check if there are pending changes here;
    // returning true will navigate without confirmation
    // returning false will show a confirm dialog before navigating away

    return true;
  }

  unloadNotification($event: any) {
    if (!this.canDeactivate()) {
      $event.returnValue = 'This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)';
    }
  }

  getAssessmentById(data, index) {
    this.loader.open();
    console.log('==')
    this.subs1 = this.api.getAssessment(data).subscribe(res => {
      this.assessmentDetail = res;
      if(res.score){
        this.score = !res.score || res.score === '0'? '': res.score;
      }
      if(res.grade){
        this.grade = res.grade;
      }
      this.questions = res.answers;
      this.changeDetectorRef.detectChanges();
      this.getMatrix();
      this.getQuestion(this.questions[index - 1]);
      setTimeout(() => {
        this.loader.close();
        this.changeDetectorRef.detectChanges();
      }, 300);
    }, err => {
      this.loader.close();
      this.confirmMsg('Fail', err.error);
    });

  }

  currentQ(qNumber, e) {
    this.currentQSelected = qNumber;
    this.getQuestion(this.questions[this.currentQSelected - 1]);
  }
  previousQ() {
    this.currentQSelected--;
    if (this.currentQSelected > 0 && this.currentQSelected < this.questions.length) {
      this.getQuestion(this.questions[this.currentQSelected - 1]);
    } else {
      this.currentQSelected++;
    }
  }
  nextQ() {
    if (this.currentQSelected > 0 && this.currentQSelected < this.questions.length) {
      this.currentQSelected++;
      this.getQuestion(this.questions[this.currentQSelected - 1]);
    }
  }
  getMatrix() {
    const matrix = [];
    const amatrix = [];
    this.questions.forEach(function (item, index) {
      matrix.push(index + 1);
      amatrix.push(item.ans);
    });
    this.questionMatrix = matrix;
    this.answerMatrix = amatrix;
  }

  getQuestion(data) {
    this.questionNumber = data.question;
    this.qAnswer = data.userAns;
    this.comments = data.comments;
    this.ans = data.ans;
  }
  // tickSelectedQ(index) {
  //     this.questions[this.currentQSelected - 1].options[index] ={}
  // }
  setAnsMatrix(): void {
    for (let index = 0; index < this.answerMatrix.length; index++) {
      if (index === this.currentQSelected - 1) {
        this.answerMatrix[index] = true;
      }
    }
  }

  doneQ() {
    if ((this.assessmentDetail || {}).status === 'UnderChecking') {
      if (this.comments) {
        this.loader.open(); 
        this.questionNumber.comments = this.comments;
        this.questionNumber.status = 'UnderChecking';
        this.questionNumber.ans = this.ans;
        this.questionNumber.resultId = this.assessmentDetail._id;
        this.subs2 = this.api.saveResult(this.questionNumber).subscribe(res => {
          this.loader.close();
          setTimeout(() => {
            this.getAssessmentById({ _id: this.ass_id }, this.currentQSelected);
          }, 50);
          this.confirmMsg('Success', 'Answer saved');
        }, err => {
          this.loader.close();
          this.confirmMsg('Fail', err.error);
        });
      }
    } else {
      this.confirmMsg('Validation', 'Assessment already Submitted!!');
    }
  }

  onComplete() {
    if ((this.assessmentDetail || {}).status === 'UnderChecking') {
      let scoreFlag = false;
      if(this.score && this.grade){
        this.confirmMsg('Validation', 'Please enter either Score or Grade.');
        return;
      } else if(this.score || this.grade){
        scoreFlag = true;
      }
      if(!scoreFlag){
        this.confirmMsg('Validation', 'Please enter either Score or Grade.');
        return;
      }
      if(this.score && this.score>100){
        this.confirmMsg('Validation', 'Score should be less than 100');
       return;
      }
      if(this.grade && this.grade !== 'A' && this.grade !== 'B' && this.grade !== 'C' && this.grade !== 'D'){
        this.confirmMsg('Validation', 'Grade should be (A/B/C/D)');
        return;
      }
      this.loader.open();
      this.subs3 = this.api.complete({ _id: this.assessmentDetail._id, status: 'Completed', type : "Essay", score: this.score, grade: this.grade}).subscribe(res => {
        this.confirmMsg('Success', 'Reviewed Successfully');
        this.loader.close();
        setTimeout(() => {
          this.getAssessmentById({ _id: this.ass_id }, this.currentQSelected);
        }, 50);
      }, err => {
        this.loader.close();
        this.confirmMsg('Fail', err.error);
      });
    } else {
      this.confirmMsg('Validation', 'Assessment already Submitted!!');
    }
  }
  errorHandler(flag){
    if(flag === 'score'){
      if(this.score>100){
        this.confirmMsg('Validation', 'Score should be less than 100');
       return;
      } else if(this.score) {
        this.grade='';
      }
    } else if(flag === 'grade'){
      if(!this.grade){
      } else if(this.grade !== 'A' && this.grade !== 'B' && this.grade !== 'C' && this.grade !== 'D'){
        this.confirmMsg('Validation', 'Grade should be (A/B/C/D)');
        return;
      } else if(this.grade) {
        this.score = '';
      }
    }
  }
  toggleSideNav() {
    this.sideNav.opened = !this.sideNav.opened;
  }
  confirmMsg(title, msg) {
    this.appear.confirm({ title: title, message: msg, button: 'close' }).subscribe(res => { });
  }
}

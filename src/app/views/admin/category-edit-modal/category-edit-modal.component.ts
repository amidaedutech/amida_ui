import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild, Inject } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from '../../../shared/services/api.service';
import { Subscription } from 'rxjs/Subscription';
import { egretAnimations } from '../../../shared/animations/egret-animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-category-edit-modal',
  templateUrl: './category-edit-modal.component.html',
  styleUrls: ['./category-edit-modal.component.scss'],
  animations: egretAnimations
})
export class CategoryEditModalComponent implements OnInit {

  public selectedImage: File;
  addCategoryForm: FormGroup;
  catDetails: any;
  constructor(private api: ApiService,
    public sanitizer: DomSanitizer,
    private loader: AppLoaderService,
    private appear: AppConfirmService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public userService: UserService,
    private cdf: ChangeDetectorRef,
    public dialogRef: MatDialogRef<CategoryEditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.addCategoryForm = this.fb.group(
        {
          name: ['', Validators.required],
          _id: [this.data],
          file: ['']
        }
      );
    }

  ngOnInit() {
    console.log('data', this.data);
    this.api.getCategoryById(this.data).subscribe(res=>{
      this.catDetails = res;
      this.addCategoryForm.patchValue({
        name: this.catDetails.name,
        _id: this.catDetails._id
      })
      this.cdf.detectChanges();
    }, err=>{
      this.confirmMsg('Fail', err.error);
    })
    // get category....
   
  }

  onImageChange(event) {
    console.log(event.target.files);
    this.selectedImage = event.target.files[0];
  }
  onAddDetails() {
      // if (this.selectedImage && this.selectedImage.name) {

      // } else {
      //   this.confirmMsg('Validation', 'Please upload image.');
      //   return;
      // }
    
    this.loader.open();
     this.api.updateCategory(this.addCategoryForm.value).subscribe(
      result => {
        // console.log('result', result);
        
          if (this.selectedImage && this.selectedImage.name) {
            this.onAddImageDetails(this.data);
          } else{
            this.dialogRef.close(true);
            this.loader.close();
            this.confirmMsg('Success', 'Details saved successfully');
          }
        
      }, err => {
        console.log('err', err);
        this.loader.close();
        this.confirmMsg('Fail', err.error);
      });
  }
  onAddImageDetails(id) {
    if (this.selectedImage && this.selectedImage.name) {
      // this.loader.open();
    this.api.uploadAWSFile(this.selectedImage).then(imagePath => {
      this.api.uploadCategoryImage({image: imagePath, categoryId: id}).subscribe(
        result => {
          this.loader.close();
          this.dialogRef.close(true);
          this.confirmMsg('Success', 'Details saved successfully');
        }, err => {
          this.loader.close();
          this.confirmMsg('Fail', err.error);
        });
    }).catch(err => {
      this.confirmMsg('Fail', err.error);
      this.loader.close();
    })
  }else {
      this.confirmMsg('Validation', 'Select image to upload');
    }
   
  }
  confirmMsg(title, msg) {
    this.appear.confirm({ title: title, message: msg, button: 'close' }).subscribe(res => { });
  }
}

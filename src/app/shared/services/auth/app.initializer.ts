import { UserService } from 'app/shared/services/user.service';
import { EndPoint } from "./../server.service";
import { HttpClient } from "@angular/common/http";

export function appInitializer(http: HttpClient, us: UserService) {
  return () => {
    const userDetails = JSON.parse(localStorage.getItem("user"));
    if ((userDetails || {})._id) {
        console.log('us', us)
      return http
        .post(EndPoint() + "users/refresh", { _id: (userDetails || {})._id })
        .toPromise()
        .then((resp: any) => {
            localStorage.setItem('access_token', resp.accessToken);
          console.log("Response 1 - ", resp);
          
        });
    } else {
      return new Promise((resolve) => {
        resolve(true);
      });
    }
  };
}
